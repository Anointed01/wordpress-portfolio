<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fwp' );

/** Database username */
define( 'DB_USER', 'Anointed1' );

/** Database password */
define( 'DB_PASSWORD', 'wordpresspass' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2!.!!(e]TL5.RUT($` E]+3~[(J2p2j*8 S2>)[i[q9Q[TTQg:cUi*)ass?Ophy]' );
define( 'SECURE_AUTH_KEY',  'I`%JXaVm-F*p)Lt7ao)_L/(5UPdi&T%)BFfMDb%d$ltkWKS[gyFcK<}|Wn>r6#UU' );
define( 'LOGGED_IN_KEY',    'Px}`M3ATbk9[K:MWIx(Tdx^`=C&#k,MRTU@p|la)<PW>em!;`cY^$se|`mDJW}Sb' );
define( 'NONCE_KEY',        '2D8G*[RM]J|8n$;BK/RWS5u+~4>u,}|,X fcA,c?LJhInJwU+%OVGCYg.6E#zK,;' );
define( 'AUTH_SALT',        '};W-ou8!kd#&`.rc~[;6vrX}WpOlhx><xGPPElT$}~$2f`>dbZf{?%baj5$BO!&o' );
define( 'SECURE_AUTH_SALT', '1EKQ=-LoZe&*vI)o-[P` pJqFQs1Ja#G@07dE1:]/SlKQ[qyDI!d1@>?k5l?L<ST' );
define( 'LOGGED_IN_SALT',   '_-5]F W<D pyo@$|YB~b`?I@>ytD[f)TX0lHt=BtRD0o(EgLnseqdA@f]?Xx65t0' );
define( 'NONCE_SALT',       '@pD@~kV/u0>h#quZm@@&s@vnw)QsZ99l+XRe^Uj@B94ge|I}oh3aYMOk>3c>26h@' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
